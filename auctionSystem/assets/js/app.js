import Vue from "vue";
import router from "./router";

import product from "./product";
import bidding from "./bidding";
Vue.component("product", product);
Vue.component("bidding", bidding);

new Vue({router}).$mount("#app");