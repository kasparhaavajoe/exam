import Vue from 'vue';
import Router from 'vue-router';
import Product from './product';
import Bidding from './bidding';

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: "/product/:id",
            name: "Bidding",
            component: Bidding,
            props: true
        }
    ]
})
