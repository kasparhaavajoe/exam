defmodule WhiteBreadContext do
  use WhiteBread.Context

  alias AuctionSystem.{Repo, Product}

  defp transform(product) do
    {price, ""} = Float.parse(product.current_price)
    %{closing_date: product.closing_date, current_price: price, description: product.description}
  end

  given_ ~r/^the following open auction are shown$/, fn state, %{table_data: table} ->
    table 
    |> Enum.map(fn product -> transform(product) end)
    |> Enum.map(fn product -> Product.changeset(%Product{}, product) end)
    |> Enum.each(fn changeset -> Repo.insert!(changeset) end)
    {:ok, state}
  end

  and_ ~r/^I want to offer "(?<bid>[^"]+)" for "(?<description>[^"]+)"$/, fn state, %{bid: bid, description: description} ->
    {bidval, ""} = Float.parse(bid)
    {:ok, state |> Map.put(:bid, bidval) |> Map.put(:description, description)}
  end

  and_  ~r/^I select the auction$/, fn state ->
    IO.inspect state
    {:ok, state}
  end

  and_ ~r/^I enter the bid$/, fn state ->
    {:ok, state}
  end

  when_ ~r/^I summit the request$/, fn state ->
    {:ok, state}
  end

  then_ ~r/the bid is accepted and price is updated accordingly^$/, fn state ->
    {:ok, state}
  end

end
