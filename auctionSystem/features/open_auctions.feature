Feature: list of open auctions
    As a user
    Such that I place bids for some product

    Scenario: accepted request
        Given the following open auction are shown
        | description                               | closing_date | current_price |
        | Chicago Bullet Speed Skate (Size 7)      | 20-01-2019   | 59.00         |
        | Riedell Dart Derby Skates (Size 8)       | 18-01-2019   | 106.00        |
        | Roller Derby Brond Blade Skate (Size 7)  | 15-01-2019   | 29.00         |
        And I want to offer "62.00" for "Chicago Bullet Speed Skate (Size 7)"
        And I select the auction
        And I enter the bid
        When I summit the request
        Then the bid is accepted and price is updated accordingly

    Scenario: rejected request
        Given the following open auctions are shown
        | description                               | closing date | current price |
        | Chicago Bullet Speed Skate (Size 7)      | 20-01-2019   | 59.00         |
        | Riedell Dart Derby Skates (Size 8)       | 18-01-2019   | 106.00        |
        | Roller Derby Brond Blade Skate (Size 7)  | 15-01-2019   | 29.00         |
        And I want to offer "50.00" for "Riedell Dart Derby Skates (Size 8)"
        And I select the auctions
        And I enter the bid
        When I summit the request
        Then the bid is rejected and price is not updated
