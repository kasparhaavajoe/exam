defmodule AuctionSystem.Product do
  use Ecto.Schema
  import Ecto.Changeset


  schema "product" do
    field :closing_date, :string
    field :current_price, :float
    field :description, :string

    timestamps()
  end

  @doc false
  def changeset(product, attrs) do
    product
    |> cast(attrs, [:description, :current_price, :closing_date])
    |> validate_required([:description, :current_price, :closing_date])
  end
end
