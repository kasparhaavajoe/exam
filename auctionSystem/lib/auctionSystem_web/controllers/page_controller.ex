defmodule AuctionSystemWeb.PageController do
  use AuctionSystemWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
