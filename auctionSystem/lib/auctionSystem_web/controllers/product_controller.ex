defmodule AuctionSystemWeb.ProductController do
    use AuctionSystemWeb, :controller

    alias AuctionSystem.{Repo, Product}

    defp transform(p) do
        %{id: p.id, description: p.description, current_price: p.current_price, closing_date: p.closing_date}
    end
  
    def index(conn, _params) do
        products = Repo.all(Product)
        |> Enum.map(fn p -> transform(p) end)
        conn
        |> json(products)
    end

    def show(conn, %{"id" => id}) do
        product = Repo.get(Product, id)
        |> transform
        conn
        |> json(product)
    end
  end
  