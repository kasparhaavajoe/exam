defmodule AuctionSystem.Repo.Migrations.CreateProduct do
  use Ecto.Migration

  def change do
    create table(:product) do
      add :description, :string
      add :current_price, :float
      add :closing_date, :string

      timestamps()
    end

  end
end
